#include "esp_camera.h"
#include "ov2640.h"
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>

#include <ArduinoMqttClient.h>      //https://github.com/arduino-libraries/ArduinoMqttClient/
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include <esp_bt.h>
#include <esp_wifi.h>
#include <esp_sleep.h>
#include <driver/rtc_io.h>

// ESP32 has two cores: APPlication core and PROcess core (the one that runs ESP32 SDK stack)
#define APP_CPU 1
#define PRO_CPU 0

// Select camera model
//#define CAMERA_MODEL_WROVER_KIT
//#define CAMERA_MODEL_ESP_EYE
//#define CAMERA_MODEL_M5STACK_PSRAM
//#define CAMERA_MODEL_M5STACK_WIDE
#define CAMERA_MODEL_AI_THINKER

#define MAX_CLIENTS   10

#include "camera_pins.h"

#define SSID1 "404_RoomNetwork"
#define PWD1 "terremotoe91tragedie"

#define LOW_POWER 0

// 1 minute
#define MQTT_RECONNECT_TIME 60000

typedef struct MqttServer{
  char userName[3];
  char key[11];
  char location[12];
  uint16_t port;
  uint8_t connAttempts;
} MqttServer;

struct ConfigurationSettings {
  char host[34];
  MqttServer mqttServer;
} ConfigurationData = {
  "",   // host
  NULL  // mqttServer
};

typedef struct MqttStatus {
  int connected;
  int state;
  uint8_t connCount;
  long int lastConnection;
} MqttStatus;

struct MqttCache {
  uint8_t activeClients;
  bool isFlashOn;
} MqttCache = {
  0,      // activeClients
  false   // isFlashOn
};

struct Status {
  uint8_t activeClients;
  bool isFlashOn;
  MqttStatus mqttStatus;
} StatusData = {
  0,      // activeClients
  false,  // isFlashOn
  NULL    // mqttStatus
};

#define DEBUG 1

#if DEBUG
  #define Sbegin(x) (Serial.begin(x))
  #define Sprintln(x) (Serial.println(x))
  #define Sprint(x) (Serial.print(x))
  #define Sprintf(x,y) (Serial.print(x))
#else
  #define Sbegin(x)
  #define Sprintln(x)
  #define Sprint(x)
#endif