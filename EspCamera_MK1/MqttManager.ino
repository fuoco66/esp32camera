long int GlobalKeepAlive = 60000;
const char GlobalAvailableTopic[] = "home/cameras/1/availability";
const char GlobalActionTopic[]  = "home/cameras/1/action";
const char GlobalStatusTopic[]  = "home/cameras/1/statusData";

String GlobalHost = String("pi");
String GlobalUserName = String("pi");
String GlobalKey = String("f4e7889@@@");
String GlobalLocation = String("192.168.1.2");

void SetupMqtt(){
  
  GlobalUserName.toCharArray(ConfigurationData.mqttServer.userName, 3);
  GlobalKey.toCharArray(ConfigurationData.mqttServer.key, 11);
  GlobalLocation.toCharArray(ConfigurationData.mqttServer.location, 12);

  ConfigurationData.mqttServer.port = 1883;
  ConfigurationData.mqttServer.connAttempts = 5;

  // sets is milliseconds
  // time to keep the connection open even if the client doesn't sends any data
  mqttClient.setKeepAliveInterval(GlobalKeepAlive);

  mqttClient.setId("Camera_1");
  mqttClient.setUsernamePassword(ConfigurationData.mqttServer.userName, ConfigurationData.mqttServer.key);
  mqttClient.setConnectionTimeout(1000);
  // mqttClient.setCleanSession(true);

  // resets connection count
  StatusData.mqttStatus.connCount = 0;

  if(ConnectMqtt() != 0){ // tries to connect
    return;
  }
}

void CheckStatusMqttThread(void * parameter){

  for(;;){ // infinite loop
    bool res = CheckMqttStatus(parameter);

    if(res){
      vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    else{
      vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

  }
  
}

bool CheckMqttStatus(void * parameter){

	//mqttClient.poll();

  // client connected, connected = 1
	if(mqttClient.connected()){
    return false;
  }
  // client Disconnected


  // check if too many attempts
  if(StatusData.mqttStatus.connCount >= ConfigurationData.mqttServer.connAttempts){

    long int timePassed = millis() - StatusData.mqttStatus.lastConnection;
    if(timePassed < MQTT_RECONNECT_TIME){
      StatusData.mqttStatus.connected = 0;
      // get last connection error
      StatusData.mqttStatus.state = mqttClient.connectError();
      return false;
    }

    // if enough time has passed, resets connection count
    Sprintln("More than 60 seconds, resets connection counter");
    StatusData.mqttStatus.connCount = 0;
  }

  // sets status to connecting
  StatusData.mqttStatus.connected = 2;

  Sprint("Attempting to reconnect to the MQTT broker: ");

  // increment connection count
  StatusData.mqttStatus.connCount = StatusData.mqttStatus.connCount + 1;
  // saves current connection time
  StatusData.mqttStatus.lastConnection = millis();
  

  // when 0, reconnected
  if(ConnectMqtt() != 0){ // try reconnecting
    return false;
  }

  StatusData.mqttStatus.connected = 1;
  StatusData.mqttStatus.state = 0;
  Sprint("Client reconnected!");
  Sprintln();
  return true;

}

int ConnectMqtt(){

  Sprint("Attempt to connect to the MQTT broker: ");
  Sprintln(ConfigurationData.mqttServer.location);
  
  SetupLastWill();

  if (!mqttClient.connect(ConfigurationData.mqttServer.location, ConfigurationData.mqttServer.port)) {
    // not connected

    int connectionError = mqttClient.connectError();
    
    Sprint("MQTT connection failed! Error code = ");
    Sprintln(connectionError);

    StatusData.mqttStatus.state = connectionError;
    
    return connectionError;
  }

  Sprint("Client connected!");
  Sprintln();
  // connected  
  StatusData.mqttStatus.connected = 1;
  StatusData.mqttStatus.state = 0;

  // resets connection count
  StatusData.mqttStatus.connCount = 0;
   
  PublishMessage(GlobalAvailableTopic, "online");
  
  SubscribeActionTopicMqtt();

  return 0;
}

void SubscribeActionTopicMqtt(){

  mqttClient.unsubscribe(GlobalActionTopic);

  mqttClient.onMessage(onMqttAction);

  Sprint("Subscribing to topic: ");
  Sprintln(GlobalActionTopic);
  Sprintln();

  // subscribe to a topic
  mqttClient.subscribe(GlobalActionTopic);
}

void onMqttAction(int messageSize) {

  String arg;
  while (mqttClient.available()) {
    arg += (char)mqttClient.read();
  }

  Sprintln("new mqtt recieved");

  // // parse argument
  DynamicJsonDocument objPayload(215);
  DeserializationError jsonError = deserializeJson(objPayload, arg);

  if(objPayload["action"] == "flashOn"){
    digitalWrite(FLASH_GPIO_NUM, HIGH);
    // {"action": "flashOn"}
  }
  else if(objPayload["action"] == "flashOff"){
    digitalWrite(FLASH_GPIO_NUM, LOW);
    // {"action": "pumpOff"}
  }

}


// void toggleLED(void * parameter){
//   for(;;){ // infinite loop

//     // Turn the LED on
//     digitalWrite(FLASH_GPIO_NUM, LOW);

//     // Pause the task for 500ms
//     vTaskDelay(1000 / portTICK_PERIOD_MS);

//     // Turn the LED off
//     digitalWrite(FLASH_GPIO_NUM, HIGH);

//     // Pause the task again for 500ms
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//   }
// }

void PublishStatusMqttThread(void * parameter){

  for(;;){ // infinite loop
    bool res = PublishStatusMqtt(parameter);

    if(res){
      vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    else{
      vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

  }
  
}

bool PublishStatusMqtt(void * parameter){
  
  if(!mqttClient.connected()){
    // disconnected, skip
    return false;
  }

  // sends data only if something has changed
  if(MqttCache.isFlashOn == StatusData.isFlashOn &&
    MqttCache.activeClients == StatusData.activeClients){
    return false;
  }

  PublishMessage(GlobalAvailableTopic, "online");
  
  // updates cache
  MqttCache.isFlashOn = StatusData.isFlashOn;
  MqttCache.activeClients = StatusData.activeClients;

  DynamicJsonDocument jsonDoc(128);
  JsonObject root = jsonDoc.to<JsonObject>();

  root["cli"] = StatusData.activeClients;
  root["fh"] = StatusData.isFlashOn == true ? "true" : "false";

  String webResponse;
  serializeJson(root, webResponse);

  // mqttClient.beginMessage("home/cameras/1/availability");
  // mqttClient.print(webResponse);
  // mqttClient.endMessage();

  PublishMessage(GlobalStatusTopic, webResponse);
  return true;

  // client connected, connected = 1
	
}

// const char GlobalAvailableTopic[] = "home/cameras/1/availability";
// const char GlobalActionTopic[]  = "home/cameras/1/action";
// const char GlobalStatusTopic[]  = "home/cameras/1/statusData";

bool PublishMessage(String topic, String payLoad){
  
  char charTopic[200], charPayload[200];
  topic.toCharArray(charTopic, 200);
  payLoad.toCharArray(charPayload, 200);
  PublishMessage(charTopic, charPayload);

}

bool PublishMessage(const char* topic, const char* payLoad){

  mqttClient.beginMessage(topic);
  mqttClient.print(payLoad);
  mqttClient.endMessage();

}

bool SetupLastWill(){
  
  const char payLoad[]  = "offline";
  
  mqttClient.beginWill(GlobalAvailableTopic, true, 0);
  mqttClient.print(payLoad);
  mqttClient.endWill();

}
