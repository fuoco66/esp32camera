#include "Hardware.h"

//Form Custom SSID
String ssidAP = "ESP_Camera_" + String((uint32_t)ESP.getEfuseMac(), HEX);//String(ESP.getChipId());

//flag for saving data
bool shouldSaveConfig = false;

const char* settingsfile = "/settings.json";
// const char* configfile = "/config.json";
uint8_t GlobalBtnLedStateForBlink = LOW;

// ESP8266WebServer ObjGlobal_HttpServer(80);
// ESP8266HTTPUpdateServer httpUpdater;
WebServer ObjGlobal_HttpServer(80);

WiFiManager ObjGlobal_WifiManager;

// WiFiClient net;
Ticker StatusLedTicker;

// TimedAction CheckStatusThread = TimedAction(500, CheckStatus);
// TimedAction BtnsThread = TimedAction(50, CheckBtns);
// TimedAction BlinkThread = TimedAction(100, BlinkButtonLed);

// TimedAction PublishMqttStatusThread = TimedAction(1000, PublishStatusMqtt);

// const char GlobalAvailableTopic[] = "home/ESP_Base/availability";
// const char GlobalActionTopic[]  = "home/ESP_Base/action";
// const char GlobalStatusTopic[]  = "home/ESP_Base/statusData";
// long int GlobalKeepAlive = 60000;

// Timezone TimeZone;

// WiFiClient WifiNetwork;
// MqttClient mqttClient(WifiNetwork);

// Status led Ticker
void tick() {
  //toggle state
  int MainLedState = digitalRead(STATUS_LED);  // get the current state of GPIO1 pin
  digitalWrite(STATUS_LED, !MainLedState);     // set pin to the opposite state
}

void setup() {

  Sbegin(115200);

  // Setup status led
  pinMode(STATUS_LED, OUTPUT);
  digitalWrite(STATUS_LED, HIGH);

  // start ticker with 0.6 because we start in AP mode and try to connect
  StatusLedTicker.attach(0.2, tick);

  ReadSettings();

  SetupConfigMode(ObjGlobal_WifiManager);
  
  // // Setup web updater
  // httpUpdater.setup(&ObjGlobal_HttpServer, SettingsData.update_path, SettingsData.update_username, SettingsData.update_password);

  // SetupWebHandlers();
  SetupHardware();
}

int LastConnectionStatus = 99;

void loop() {
  // this seems to be necessary to let IDLE task run and do GC
  vTaskDelay(1000);
  // events();


  // CheckStatusThread.check();

  // BtnsThread.check();
  // BlinkThread.check();

  // PublishMqttStatusThread.check();
  
  // PublishMqttAmbientThread.check();
  
  // int connectionStatus = WiFi.status();

  // // connection status has changed, and is not connected, reboot
  // if(connectionStatus != LastConnectionStatus &&
  //    connectionStatus != 3){
  //   Sprint("Connection lost, code: ");
  //   Sprintln(connectionStatus);
  //   delay(5000);
  // 	ESP.reset();

  // }
  
  // LastConnectionStatus = connectionStatus;

}

