// #include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
// #include <DNSServer.h>
// #include <ESP8266WebServer.h>
// #include <ESP8266HTTPClient.h>
// #include <WiFiClientSecure.h> 
#include "FS.h"
#include <SPIFFS.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>

// #include <ESP8266mDNS.h>
// #include <ESP8266HTTPUpdateServer.h>
#include <Ticker.h>

#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "esp_camera.h"
#include "ov2640.h"

#include <esp_bt.h>
#include <esp_wifi.h>
#include <esp_sleep.h>
#include <driver/rtc_io.h>

// ESP32 has two cores: APPlication core and PROcess core (the one that runs ESP32 SDK stack)
#define APP_CPU 1
#define PRO_CPU 0

#define CAMERA_MODEL_AI_THINKER

#define MAX_CLIENTS   10

// #define SSID1 "404_RoomNetwork"
#define PWD1 "terremotoe91tragedie"

#define LOW_POWER 0

// 1 minute
// #define MQTT_RECONNECT_TIME 60000

#define STATUS_LED  33
#define LED_GPIO_NUM      33

struct Settings {

	char host[34];
	char update_username[40];
	char update_password[40];
	char update_path[34];

} SettingsData = {
  // The default values
	"",
  "",
  "",
  "/firmware"
};

// struct ConfigurationSettings {
//   bool genericBool;
//   // uint8_t genericInt;
//   // long int genericLong;
//   MqttServer mqttServer;
// } ConfigurationData = {
//    false, // genericBool
//   //  0,     // genericInt
//   //  0,     // genericLong
//   NULL       // mqttServer
// };

struct Status {
  uint8_t activeClients;
  bool isFlashOn;
  // MqttStatus mqttStatus;
} StatusData = {
  0,      // activeClients
  false//,  // isFlashOn
  // NULL    // mqttStatus
};

void ReadSettings();
// void CheckStatus();
// void CheckBtns();
// void BlinkButtonLed();
// bool InitAmbientSensor();

#define DEBUG 1

#if DEBUG
  #define Sbegin(x) (Serial.begin(x))
  #define Sprintln(x) (Serial.println(x))
  #define Sprint(x) (Serial.print(x))
  #define Sprintf(x,y) (Serial.print(x))
#else
  #define Sbegin(x)
  #define Sprintln(x)
  #define Sprint(x)
#endif
