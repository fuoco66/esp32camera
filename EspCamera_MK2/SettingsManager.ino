void ReadSettings(){

	if (SPIFFS.begin()) {
		Sprintln("Settings Spiff begin");

		if (SPIFFS.exists(settingsfile)) {
			
			Sprintln("Settings File found");
			
			//file exists, reading and loading
			File SettingsFile = SPIFFS.open(settingsfile, "r");

			if (SettingsFile) {
				Sprintln("Settings File opened");

				size_t size = SettingsFile.size();
				// Allocate a buffer to store contents of the file.
				std::unique_ptr<char[]> buf(new char[size]);

				SettingsFile.readBytes(buf.get(), size);
				// DynamicJsonBuffer jsonBuffer;
				DynamicJsonDocument jsonBuffer(size);
				// JsonObject json = jsonBuffer.parseObject(buf.get());
				
				DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
			
				if (jsonError){
					Sprintln("failed to load json Settings");
					SettingsFile.close();
  				return;
				}

				Sprintln("");
				// print JSON to serial
				// serializeJson(jsonBuffer, Serial);
				Sprintln("");

				Sprintln("Load settings");

				strcpy(SettingsData.host, jsonBuffer["host"]);
				Sprintln("Loadaded host");

				strcpy(SettingsData.update_username, jsonBuffer["update_username"]);
				Sprintln("Loadaded update_username");

				strcpy(SettingsData.update_password, jsonBuffer["update_password"]);
				Sprintln("Loadaded update_password");

				strcpy(SettingsData.update_path, jsonBuffer["update_path"]);
				Sprintln("Loadaded update_path");

				// close file
				SettingsFile.close();

			}
		}
		else{
			Sprintln("File NOT found");
		}
	} 
	else {
		Sprintln("SPIFF FAILED");
	}
}

void SaveSettings(){

	DynamicJsonDocument jsonDoc(1024);
	jsonDoc["host"] = SettingsData.host;

	jsonDoc["update_username"] = SettingsData.update_username;
	jsonDoc["update_password"] = SettingsData.update_password;
	jsonDoc["update_path"] = SettingsData.update_path;

	File SettingsFile = SPIFFS.open(settingsfile, "w");
	if (!SettingsFile) {
	}

	// serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, SettingsFile);

	SettingsFile.close();
}

// bool CreateConfigFile(){
	
// 	DynamicJsonDocument jsonDoc(1024);
// 	JsonObject root = jsonDoc.to<JsonObject>();

// 	// load default data from struct init	
// 	root["genericBool"] = ConfigurationData.genericBool;

// 	JsonObject mqttServer = root.createNestedObject("mqttServer");

// 	mqttServer["userName"] = ConfigurationData.mqttServer.userName;
// 	mqttServer["key"] = ConfigurationData.mqttServer.key;
// 	mqttServer["location"] = ConfigurationData.mqttServer.location;
// 	mqttServer["port"] = ConfigurationData.mqttServer.port;
// 	mqttServer["connAttempts"] = ConfigurationData.mqttServer.connAttempts;

// 	File ConfigFile = SPIFFS.open(configfile, "w");
// 	if (!ConfigFile) {
// 		Sprintln("Failed to create file");
// 		return false;
// 	}

// 	serializeJson(root, Serial);
// 	serializeJson(root, ConfigFile);

// 	ConfigFile.close();
// 	Sprintln("File created");

// 	return true;
// }

// // Load the buttons config from file
// void LoadConfig(){

// 	if (!SPIFFS.begin()) {
// 		Sprintln("Config SPIFF FAILED");
// 		return;
// 	}
	
// 	Sprintln("Config Spiff begin");

// 	if(!SPIFFS.exists(configfile)) {
// 		Sprintln("File NOT found, creating");
// 		if(CreateConfigFile() != true){
// 			return;
// 		}
// 	}
		
// 	Sprintln("Config File found");
	
// 	//file exists, reading and loading
// 	File objConfigFile = SPIFFS.open(configfile, "r");

// 	if(!objConfigFile){
// 		Sprintln("Failed to open Button file");
// 		return;
// 	}

// 	// Sprintln("Button File opened");
// 	// for debug, print the whole file
// 	// while (objConfigFile.available()) {

// 	//     Serial.write(objConfigFile.read());
// 	// }

// 	size_t size = objConfigFile.size();
// 	// Allocate a buffer to store contents of the file.
// 	std::unique_ptr<char[]> buf(new char[size]);

// 	objConfigFile.readBytes(buf.get(), size);

// 	DynamicJsonDocument jsonBuffer(1024);
// 	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
// 	if (jsonError){
// 		Sprintln("Failed to parse config json: ");
// 		switch (jsonError.code()) {
// 			case DeserializationError::InvalidInput:
// 					Sprintln(F("Invalid input!"));
// 					break;
// 			case DeserializationError::NoMemory:
// 					Sprintln(F("Not enough memory"));
// 					break;
// 			default:
// 					Sprintln(F("Deserialization failed"));
// 					break;
// 		}

// 		objConfigFile.close();
// 		return;
// 	}

// 	Sprintln("config File Loaded");

// 	ConfigurationData.genericBool = jsonBuffer["genericBool"].as<bool>();
	
// 	strcpy(ConfigurationData.mqttServer.userName, jsonBuffer["mqttServer"]["userName"].as<char*>());
// 	strcpy(ConfigurationData.mqttServer.key, jsonBuffer["mqttServer"]["key"].as<char*>());
// 	strcpy(ConfigurationData.mqttServer.location, jsonBuffer["mqttServer"]["location"].as<char*>());
// 	ConfigurationData.mqttServer.port = jsonBuffer["mqttServer"]["port"];
// 	ConfigurationData.mqttServer.connAttempts = jsonBuffer["mqttServer"]["connAttempts"];


// }

// // load the config from memory
// DynamicJsonDocument WebGetConfig(){

// 	DynamicJsonDocument jsonDoc(1024);
// 	JsonObject root = jsonDoc.to<JsonObject>();

// 	root["genericBool"] = ConfigurationData.genericBool;
		
// 	JsonObject mqttServer = root.createNestedObject("mqttServer");

// 	mqttServer["userName"] = ConfigurationData.mqttServer.userName;
// 	mqttServer["key"] = ConfigurationData.mqttServer.key;
// 	mqttServer["location"] = ConfigurationData.mqttServer.location;
// 	mqttServer["port"] = ConfigurationData.mqttServer.port;
// 	mqttServer["connAttempts"] = ConfigurationData.mqttServer.connAttempts;


// 	return jsonDoc;

// }

// void SaveConfigFile(String payLoad, String strFileName){

// 	DynamicJsonDocument jsonBuffer(2048);
// 	DeserializationError jsonError = deserializeJson(jsonBuffer, payLoad);

// 	if (jsonError){
// 		Sprintln("Failed to parse json");
// 		return;
// 	}

// 	File objFile = SPIFFS.open(strFileName, "w");
// 	if (!objFile) {
// 		Sprintln("Failed to open file");
// 		return;
// 	}

// 	Sprintln("Writing file");
// 	// serializeJson(jsonBuffer["value"], Serial);

// 	serializeJson(jsonBuffer["value"], objFile);
// 	Sprintln("File Written");

// 	objFile.close();
		
// 	Sprintln("Writing END");
	
// }

void WifiReset(){
	ObjGlobal_WifiManager.resetSettings();
	ESP.restart();
}

void FullReset(){

	if (SPIFFS.begin()) {
		// TODO implement better way
		if (SPIFFS.exists(settingsfile)) {
			SPIFFS.remove(settingsfile);
		}
		
		// if (SPIFFS.exists(configfile)) {
		// 	SPIFFS.remove(configfile);
		// }

		SPIFFS.end();

	}
  WifiReset();
}

void Reboot(){
	ESP.restart();
}
