#include "camera_pins.h"

void SetupHardware(){
	// Setup Serial connection:
  Serial.printf("setup: free heap  : %d\n", ESP.getFreeHeap());

  // Configure the camera
  //  camera_config_t config;
  //  config.ledc_channel = LEDC_CHANNEL_0;
  //  config.ledc_timer = LEDC_TIMER_0;
  //  config.pin_d0 = Y2_GPIO_NUM;
  //  config.pin_d1 = Y3_GPIO_NUM;
  //  config.pin_d2 = Y4_GPIO_NUM;
  //  config.pin_d3 = Y5_GPIO_NUM;
  //  config.pin_d4 = Y6_GPIO_NUM;
  //  config.pin_d5 = Y7_GPIO_NUM;
  //  config.pin_d6 = Y8_GPIO_NUM;
  //  config.pin_d7 = Y9_GPIO_NUM;
  //  config.pin_xclk = XCLK_GPIO_NUM;
  //  config.pin_pclk = PCLK_GPIO_NUM;
  //  config.pin_vsync = VSYNC_GPIO_NUM;
  //  config.pin_href = HREF_GPIO_NUM;
  //  config.pin_sscb_sda = SIOD_GPIO_NUM;
  //  config.pin_sscb_scl = SIOC_GPIO_NUM;
  //  config.pin_pwdn = PWDN_GPIO_NUM;
  //  config.pin_reset = RESET_GPIO_NUM;
  //  config.xclk_freq_hz = 20000000;
  //  config.pixel_format = PIXFORMAT_JPEG;
  //
  //  // Frame parameters: pick one
  //  //  config.frame_size = FRAMESIZE_UXGA;
  //  //  config.frame_size = FRAMESIZE_SVGA;
  //  //  config.frame_size = FRAMESIZE_QVGA;
  //  config.frame_size = FRAMESIZE_VGA;
  //  config.jpeg_quality = 12;
  //  config.fb_count = 2;

  static camera_config_t camera_config = {
    .pin_pwdn       = PWDN_GPIO_NUM,
    .pin_reset      = RESET_GPIO_NUM,
    .pin_xclk       = XCLK_GPIO_NUM,
    .pin_sscb_sda   = SIOD_GPIO_NUM,
    .pin_sscb_scl   = SIOC_GPIO_NUM,
    .pin_d7         = Y9_GPIO_NUM,
    .pin_d6         = Y8_GPIO_NUM,
    .pin_d5         = Y7_GPIO_NUM,
    .pin_d4         = Y6_GPIO_NUM,
    .pin_d3         = Y5_GPIO_NUM,
    .pin_d2         = Y4_GPIO_NUM,
    .pin_d1         = Y3_GPIO_NUM,
    .pin_d0         = Y2_GPIO_NUM,
    .pin_vsync      = VSYNC_GPIO_NUM,
    .pin_href       = HREF_GPIO_NUM,
    .pin_pclk       = PCLK_GPIO_NUM,

    .xclk_freq_hz   = 20000000,
    .ledc_timer     = LEDC_TIMER_0,
    .ledc_channel   = LEDC_CHANNEL_0,
    .pixel_format   = PIXFORMAT_JPEG,
    /*
        FRAMESIZE_96X96,    // 96x96
        FRAMESIZE_QQVGA,    // 160x120
        FRAMESIZE_QCIF,     // 176x144
        FRAMESIZE_HQVGA,    // 240x176
        FRAMESIZE_240X240,  // 240x240
        FRAMESIZE_QVGA,     // 320x240
        FRAMESIZE_CIF,      // 400x296
        FRAMESIZE_HVGA,     // 480x320
        FRAMESIZE_VGA,      // 640x480
        FRAMESIZE_SVGA,     // 800x600
        FRAMESIZE_XGA,      // 1024x768
        FRAMESIZE_HD,       // 1280x720
        FRAMESIZE_SXGA,     // 1280x1024
        FRAMESIZE_UXGA,     // 1600x1200
    */
    //    .frame_size     = FRAMESIZE_QVGA,
    //    .frame_size     = FRAMESIZE_UXGA,
       .frame_size     = FRAMESIZE_SVGA,
    //    .frame_size     = FRAMESIZE_VGA,
    // .frame_size     = FRAMESIZE_UXGA,
    .jpeg_quality   = 16,
    .fb_count       = 2
  };

#if defined(CAMERA_MODEL_ESP_EYE)
  pinMode(13, INPUT_PULLUP);
  pinMode(14, INPUT_PULLUP);
#endif

  if (esp_camera_init(&camera_config) != ESP_OK) {
    Serial.println("Error initializing the camera");
    delay(10000);
    ESP.restart();
  }

  sensor_t* s = esp_camera_sensor_get();
//  s->set_vflip(s, true);
    s->set_vflip(s, false);

  // //  Configure and connect to WiFi
  IPAddress ip;

  // WiFi.mode(WIFI_STA);
  // WiFi.begin(SSID1, PWD1);
  // Serial.print("Connecting to WiFi");

  // while (WiFi.status() != WL_CONNECTED){
  //   delay(500);
  //   Serial.print(F("."));
  // }

  ip = WiFi.localIP();
  Serial.println(F("WiFi connected"));
  Serial.println("");
  Serial.print("Stream Link: http://");
  Serial.print(ip);
  Serial.println("/mjpeg/1");

  // init Flash and status led
  pinMode(FLASH_GPIO_NUM, OUTPUT);
  // pinMode(LED_GPIO_NUM, OUTPUT);

  // Start mainstreaming RTOS task
  xTaskCreatePinnedToCore(
    mjpegCB,
    "mjpeg",
    2*1024,
    NULL,
    2,
    &tMjpeg,
    APP_CPU);
  
  Serial.printf("setup complete: free heap  : %d\n", ESP.getFreeHeap());
}

void ReloadStatus(){

	// int isPumpActive;

	// if(!ConfigurationData.pumpPinInverted){
	//   isPumpActive = digitalRead(ConfigurationData.pumpPin);
	// }
	// else{
	//   isPumpActive = !digitalRead(ConfigurationData.pumpPin);
	// }

	// StatusData.isPumpActive = isPumpActive;
	
	// GetPortExpanderStatus();
	
	// StatusData.isWaterLow = isWaterLow(StatusData.waterLevel);
	
	// ReloadAmbientData();
	
	// CheckMqttStatus();

}

// DynamicJsonDocument WebGetCachedStatus(){

// 	DynamicJsonDocument jsonDoc(1024);
// 	JsonObject root = jsonDoc.to<JsonObject>();

// 	JsonObject mqttStatus = root.createNestedObject("mqttStatus");

// 	mqttStatus["connected"] = StatusData.mqttStatus.connected;
// 	mqttStatus["state"] = StatusData.mqttStatus.state;
// 	mqttStatus["connCount"] = StatusData.mqttStatus.connCount;

// 	return jsonDoc;

// }

void CheckStatus(){

	// ensure that the sensors status is always updated
	ReloadStatus();
	

}
