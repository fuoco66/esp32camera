
$(document).ready(function(){

  	//start manage loader
	$(document).ajaxSend(function(event, jqxhr, settings) {
    //default loader used
    if(settings.dataSkiploader){
      return;
    }
		$('#generalLoader').fadeIn(150);
	});

	$(document).ajaxComplete(function(event, jqxhr, settings) {
    $('#generalLoader').fadeOut(300);			
	});
  //end manage loader
  
  ReloadAll();

  // setInterval(function(){
  //   GetStatus(false, true)
  // }, 1000);

});

var checkEventIgnore = false;

var Paths = {
  osConfig: '/osConfig',
  osSystem: '/osSystem',
  osAction: '/osAction',
  osStatus: '/osStatus'
}

var GlobalConfigData = null

var GaugeCanvas = null

function ReloadAll(){
  GetConfig();
  GetStatus(true, false);
}

function pad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length-size);
}

function osSystem(action) {

  var dataJSON = {
    action: action
  }

  $.ajax({
    type: "POST",
    url: Paths.osSystem,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      location.reload();
    
    },
    error: function(jqXHR,error, errorThrown) { 
      alert("Operation failed"); 
      console.log("Something went wrong");
      
    }
  });

}

function GetConfig() {

  var dataJSON = {
    action: "getConfig"
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderConfig(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
    }
  });
}

function RenderConfig(configData){
  
  // saves configuration
  GlobalConfigData = configData;

  var pinContainer = $('#mainConfig_PumpPin').html('');
   
  $('#mqttServerConfig_userName').val(configData.mqttServer.userName);
  $('#mqttServerConfig_key').val(configData.mqttServer.key);
  $('#mqttServerConfig_location').val(configData.mqttServer.location);
  $('#mqttServerConfig_port').val(configData.mqttServer.port);
  $('#mqttServerConfig_connAttempts').val(configData.mqttServer.connAttempts);

// mqttServerConfig_connCount
}

function SaveConfigData(){

  // var pumpPin = $('#mainConfig_PumpPin').val();

  let mqttUserName = $('#mqttServerConfig_userName').val();
  let mqttKey = $('#mqttServerConfig_key').val();
  let mqttLocation = $('#mqttServerConfig_location').val();
  let mqttPort = $('#mqttServerConfig_port').val();
  let mqttConnAttempts = $('#mqttServerConfig_connAttempts').val();

  // get data
  var ConfigData = {
    genericBool: true, //genericBool,
    mqttServer:{
      userName: mqttUserName,
      key: mqttKey,
      location: mqttLocation,
      port: mqttPort,
      connAttempts: mqttConnAttempts
    }
  };

  SaveConfigDataWebCall(ConfigData);
}

function SaveConfigDataWebCall(ConfigData){

  var dataJSON = {
    action: 'saveConfig',
    value: ConfigData
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      // reload interface
      RenderConfig(msg.response);
      
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

function minutesAndSecondsToMilliseconds(min,sec){
  return((min*60+sec)*1000);
}

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  
  var returnObj = {
    minutes: minutes,
    seconds: (seconds < 10 ? '0' : '') + seconds  
  }

  return  returnObj;
}

// this will be called every second
function GetStatus(reload, skipLoader) {

  var dataJSON = {
    action: 'GetStatus'
  }

  if(reload == true){
    dataJSON.action = 'GetReloadStatus';
  }

  $.ajax({
    type: "POST",
    url: Paths.osStatus,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: skipLoader,
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

// this will be called every second
function RenderStatus(statusData){
  
  // // sensor and Pump status
  // var objPumpStatus = $('#status_isPumpOn');
  // var sensorStatus = $('#status_sensorTriggered');

  // objPumpStatus.val(statusData.isPumpActive);
  // sensorStatus.val(statusData.isWaterLow);
  
  // // disable onchange event
  // checkEventIgnore = true;
  // $('#status_remoteSensorOverride').val(statusData.sensorOverride);
  // $('#status_overrideSensor').prop("checked", statusData.sensorOverride);
  // checkEventIgnore = false;

  // var elapsedTime = millisToMinutesAndSeconds(statusData.elapsedTime);
  
  // $('#timed_elapsedTime').val(elapsedTime.minutes + ':' + elapsedTime.seconds);
  // $('#manual_elapsedTime').val(elapsedTime.minutes + ':' + elapsedTime.seconds);
  
  // // enable/disable all buttons
  // var overrideSensor = statusData.sensorOverride;//$('#status_overrideSensor').prop("checked");
  
  // UpdateWaterGauge(statusData.waterLevel);

  // // to change, add error variable to backend
  // if((statusData.machineState == 0 || statusData.machineState == 1) || overrideSensor){
  //   HidePills('.status_statusPillError');
  //   $('.controlBtn').prop('disabled', false);
  // }
  // else{
  //   // error state, 
  //   $('.status_statusPillError').html(ConvertMachineStateToH(statusData.machineState));
      
  //   HidePills('.status_statusPillOn, .status_statusPillReady');
  //   ShowPills('.status_statusPillError');
  //   $('.controlBtn').prop('disabled', true);
  //   return
  // }
  
  // var btnManualOn = $('#manual_onButton');
  // var btnManualOff = $('#manual_offButton');
  // var btntimedOn = $('#timed_onButton');

  // if(!statusData.isPumpActive){
  //   HidePills('.status_statusPillOn');
  //   ShowPills('.status_statusPillReady');
  //   btnManualOn.prop('disabled', false);
  //   btntimedOn.prop('disabled', false);
  //   btnManualOff.prop('disabled', true);
  // }
  // else{
  //   HidePills('.status_statusPillReady');
  //   ShowPills('.status_statusPillOn');
  //   btnManualOn.prop('disabled', true);
  //   btntimedOn.prop('disabled', true);
  //   btnManualOff.prop('disabled', false);
  // }

  // $('#ambientData_temperature').html(Math.round(statusData.ambientData.temperature));
  // $('#ambientData_humidity').html(Math.round(statusData.ambientData.humidity));
  // $('#ambientData_pressure').html(Math.round(statusData.ambientData.pressure));
  // $('#ambientData_psuTemperature').html(Math.round(statusData.ambientData.psuTemperature));

  // // connecting
  // if(statusData.mqttStatus.connected == 2){
  //   HidePills('.mqttServerConfig_connected');
  //   HidePills('.mqttServerConfig_disconnected');
  //   ShowPills('.mqttServerConfig_connecting');
  //   $('#mainConfig_mqttConnect').prop('disabled', true);

  //   $('#mqttServerConfig_connCount').html(statusData.mqttStatus.connCount);
  //   ShowPills('#mqttServerConfig_connecting');

  //   return;
  // }

  // mqtt disconnected
  if(statusData.mqttStatus.connected == 0){
    HidePills('.mqttServerConfig_connected');
    HidePills('.mqttServerConfig_connecting');
    ShowPills('.mqttServerConfig_disconnected');
    $('#mainConfig_mqttConnect').prop('disabled', false);
    HidePills('#mqttServerConfig_connCount');
  }
  else{
    HidePills('.mqttServerConfig_disconnected');
    HidePills('.mqttServerConfig_connecting');
    ShowPills('.mqttServerConfig_connected');
    $('#mainConfig_mqttConnect').prop('disabled', true);
    HidePills('#mqttServerConfig_connCount');
  }

}


function ShowPills(pills){
  $(pills).show();
}

function HidePills(pills){
  $(pills).hide();
}


function ConnectMqtt(action){

  var dataJSON = {
    action: 'mqttConnect'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}