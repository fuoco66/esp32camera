Paste all the files in the Driver folder in this path (sort of, adjust first part, and core version)
C:\Users\emanu\AppData\Local\Arduino15\packages\esp32\hardware\esp32\1.0.6\tools\sdk\include\esp32-camera

PREFERRED SKETCH: esp32-cam-rtos
The Full Frame, has just a bigger image (Slow)

PIN 4 -> FLASH
PIN 33 -> RED LED

compile like this
Board: AI-Thinker ESP32-CAM or ESP-EYE
Compile as:
  ESP32 Dev Module
  CPU Freq: 240
  Flash Freq: 80
  Flash mode: QIO
  Flash Size: 4Mb
  Partrition: Minimal SPIFFS
  PSRAM: Enabled